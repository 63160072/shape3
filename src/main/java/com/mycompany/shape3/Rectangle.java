/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape3;

/**
 *
 * @author sippu
 */
public class Rectangle extends Shape {

    private double l;
    private double w;

    @Override
    public double calArea() {
        return l * w;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "l=" + l + ",w=" + w + '}';
    }

    public Rectangle(double l, double w) {
        super("Rectangle");
        this.l = l;
        this.w = w; 
    }

    public void setL(double l) {
        this.l = l;
    }

    public void setW(double w) {
        this.w = w;
    }

    public double getL() {
        return l;
    }

    public double getW() {
        return w;
    }

}
