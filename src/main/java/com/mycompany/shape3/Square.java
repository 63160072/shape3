/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape3;

/**
 *
 * @author sippu
 */
public class Square extends Shape {

    private double s;

    @Override
    public double calArea() {
        return s * s;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "s=" + s + '}';
    }

    public Square(double s) {
        super("Square");
        this.s = s;
    }

    public void setS(double s) {
        this.s = s;
    }

    public double getS() {
        return s;
    }

}
